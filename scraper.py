from random import randint
import aiohttp
import asyncio
import time
import os


NATIONAL = 'https://data.weatherzone.com.au/data/timestamped/images/radar/anims/twc15/radar_wzcountry_aus_640x480/radar_wzcountry_aus_640x480.{}.png'
DEFAULT_URL = NATIONAL

OK = 200
DEFLT_TIME_ITER = 10 * 60 #Default 10 minutes
#Offset of 10h as weatherzone data is in UTC
OFFSET = 10 * 60 * 60
DEBUG = True    #Debug flag, set true to print debugging messages


def dprint(msg):
    '''
    Debugging print. Prints a debugging message only if DEBUG is set to True
    '''
    if DEBUG:
        print('DEBUG: ' + msg)


def stamp_to_unix(timestamp):
    '''
    Takes a timestamp in the form: %Y%m%d%H%M and returns a unix timestamp
    '''
    return time.mktime(time.strptime(timestamp, '%Y%m%d%H%M'))


def unix_to_stamp(unix_time):
    '''
    Takes in unix time and creates a timestamp in the form: %Y%m%d%H%M
    '''
    return time.strftime('%Y%m%d%H%M', time.localtime(unix_time))


def utc_to_aest(stamp):
    '''
    Converts a UTC timestamp into an AEST timestamp
    '''
    unix = stamp_to_unix(stamp)
    unix += OFFSET
    return unix_to_stamp(unix)


def aest_to_utc(stamp):
    '''
    Converts a AEST timestamp into an UTC timestamp
    '''
    unix = stamp_to_unix(stamp)
    unix += OFFSET
    return unix_to_stamp(unix)


def add_iter_to_stamp(timestamp, _iter):
    '''
    Adds the interation time to a timestamp in the form: %Y%m%d%H%M
    '''
    stamp_unix = stamp_to_unix(timestamp)
    stamp_unix += _iter
    return time.strftime('%Y%m%d%H%M', time.localtime(stamp_unix))


async def scrape(directory='./', url=DEFAULT_URL, start=None, end=10**11, _iter=DEFLT_TIME_ITER):
    '''
    directory: String. The directory where the images are to be stored.
                Must have a trailing slash.
    start: String. Timestamp in the form: %Y%m%d%H%M. If none provided, current
                time minus offset is calculated.
    end: Int. The unix timestamp of when to stop scraping. If none provided,
                scraping will run forever.
    _iter: The time between images released by the site.
    '''
    #Set current_time to current time if start is None
    if start is None:
        current_time = aest_to_utc(time.strftime('%Y%m%d%H%M', time.localtime(time.time()-OFFSET)))
    else:
        current_time = aest_to_utc(start)
    #Adds a leading slash if there is none in the provided directory
    if not directory.endswith('/'):
        directory += '/'

    #Keep going until we reach the end
    while (stamp_to_unix(current_time) < end):
        #Get as many images in history as possible
        while (stamp_to_unix(current_time) < time.time()-OFFSET):
            current_time_aest = utc_to_aest(current_time)
            current_img_file = directory + current_time_aest + '.png'
            #Do not make a web query if the file exists
            if os.path.isfile(current_img_file):
                current_time = add_iter_to_stamp(current_time, _iter)
                continue

            dprint('Processing {} (UTC) for {}'.format(current_time, directory))

            async with aiohttp.ClientSession() as session:
                async with session.get(url.format(current_time)) as resp:
                    dprint('GET request executed for {} (UTC) status: {}'.format(current_time, resp.status))
                    try:
                        #Test if the response is OK
                        assert resp.status == OK
                        #Prevent crawling by 1 min if OK
                        fetch_success = True
                        #Get the raw image data
                        img = await resp.read()
                        #Write it to file in binary mode
                        with open(current_img_file, 'wb') as img_file:
                            img_file.write(img)
                        dprint('Wrote file {}'.format(current_img_file))
                    except:
                        dprint('\t\tERROR: Failed to fetch data at {} (UTC) for region {}'.format(current_time, directory))
                        dprint('\t\tURL: {}'.format(url.format(current_time)))
                        #Crawl by 1 minute at a time
                        fetch_success = False

            #If failed to fetch image, switch to 1 minute intervals until fetch success
            if fetch_success:
                next_iter = _iter
            else:
                next_iter = 60 #1min

            current_time = add_iter_to_stamp(current_time, next_iter)
            #Random wait time to stagger async requests and not spam servers
            await asyncio.sleep(randint(5, 10))

        #Fixes the edge case where the servers have not stored an image at the latest interval
        #Moves back 10 intervals and waits one interval for the new image
        if not fetch_success:
            dprint('Moving time back {} seconds'.format(_iter*10))
            current_time = add_iter_to_stamp(current_time, -_iter*10)
        await asyncio.sleep(_iter)
