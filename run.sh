#!/bin/bash

#
# NOTICE:
# This script is now deprecated in favour of seperated execution
# of individual scripts. This code may not work.
#

export FLASK_APP=api.py
export FLASK_ENV=development

#Note, this will fork the scraper to a background task
python run.py &
pid1=$!
flask run &
pid2=$!


sleep 2
echo
echo
echo "API and scraper now running, press enter to exit."
read

kill -15 $pid1
kill -15 $pid2
