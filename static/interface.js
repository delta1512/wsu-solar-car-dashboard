const currentDate = new Date()
const currentUnix = currentDate.getTime() / 1000
//Change these to literal constants later
const minTime = currentUnix - 7 * 24 * 60 * 60  //Minus 1 week
const maxTime = currentUnix


function setup() {
  //Setup the slider with min and max values
  var slider = document.getElementById("time-slider")
  slider.min = minTime
  slider.max = maxTime
  slider.value = minTime
  slider.onchange = getNewOverlay
}


function displayTimestamp(t) {
  var dateObj = new Date(t * 1000)
  var textObj = document.getElementById("timestamp")

  var year = dateObj.getFullYear()
  //Month is offset by 1 for some reason
  var month = dateObj.getMonth() + 1
  month = month < 10 ? '0' + month : month
  var date = dateObj.getDate() < 10 ? '0' + dateObj.getDate() : dateObj.getDate()
  var hour = dateObj.getHours() < 10 ? '0' + dateObj.getHours() : dateObj.getHours()
  var min = dateObj.getMinutes() < 10 ? '0' + dateObj.getMinutes() : dateObj.getMinutes()

  textObj.innerHTML = date + "/" + month + "/" + year + " " + hour + ":" + min
}
