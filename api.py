from glob import glob
from flask import Flask, request, render_template, jsonify

from scraper import stamp_to_unix, unix_to_stamp
import config as conf
import areas as a


### CONSTANTS
app = Flask(__name__)
BAD_REQUEST = 400

def find_closest_stamp(stamps, target):
    '''
    Finds the closest number in a set given a target number.
    If the difference between the result and the original number
    is greater than the threshold provided, then it will return
    a null value.

    All values must be in unix time.
    '''

    THRESH = 3600 # 1 hour max difference

    delta_stamps = [stamp - target for stamp in stamps]
    abs_delta_stamps = [abs(stamp) for stamp in delta_stamps]
    smallest_diff = min(abs_delta_stamps)
    if smallest_diff < THRESH:
        #Index delta_stamps based off of the index of smallest_diff in the
        #array of absolute diffs.
        smallest_delta = delta_stamps[abs_delta_stamps.index(smallest_diff)]
        #Reverse the operation
        return smallest_delta + target
    else:
        return 0


def get_overlay_json_data(req_time):
    areas = list(a.AREAS.keys())
    radar = {}
    clouds = {}
    lightning = {}
    latlngs = {}

    for area in areas:
        area_dir = a.get_area_dir(area, type='radar') + '/'
        image_files = glob(area_dir + '*.png')
        # Inline loop to get the numbers from the image file names, then turn them to unix stamps
        img_stamps = [stamp_to_unix(name.replace(area_dir, '').replace('.png', '')) for name in image_files]
        match = find_closest_stamp(img_stamps, req_time)
        radar[area] = area_dir + unix_to_stamp(match) + '.png'
        latlngs[area] = a.get_area_latlng(area)

    return jsonify({
        'radar'     :   radar,
        'cloud'     :   clouds,
        'lightning' :   lightning,
        'areas'     :   areas,
        'latlngs'   :   latlngs
    })


@app.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('dashboard.html', host=conf.HOSTNAME, map_api=conf.MAP_API_KEY)


@app.route('/getimages', methods=['GET'])
def get_image():
    try:
        #time must be a unix timestamp
        t = int(request.args.get('t', None))
        assert not t is None
    except (AssertionError, ValueError):
        return '', BAD_REQUEST

    return get_overlay_json_data(t)


#Run as a regular python script
if __name__ == "__main__":
    app.run(port=conf.PORT)
