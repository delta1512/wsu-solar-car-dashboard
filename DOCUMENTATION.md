# Documentation for WSU Solar Car Dashboard

## Contents

1. Introduction and Scope
2. System Flowchart
3. File Descriptions
4. Adding New Imagery


## 1. Introduction and Scope

This repository contains a set of programs that collect image data over the internet and display it to a user on a map. Images are dated and users are able to select which time interval of images are to be displayed.

This code is based around Flask for Python which is an API and web-server combined. Flask handles page serving and is responsible for providing dynamic content over and API backend.

The system can:
* Collect images from the internet on a regular basis and store them
* Serve images based on a requested time
* Overlay images onto a map via a web interface and Google maps


## 2. System Flowchart

![](./static/img/system_flowchart.png)


## 3. File Descriptions

### api.py: The Flask application file (must run this file)

This is the main file that runs the Flask server. It serves the dashboard webpage (`/dashboard`) which includes the Google Map and provides the imagery (`/getimage`).

The way images are provided is as follows:

1. Client requests images at a timestamp (eg, `/getimage?t=1562321168`).
2. api.py looks for the images in the configured directories closest to this timestamp (using the `find_closest_stamp()` method) for every area in [areas.json](./areas.json). It will find images within 1hr past or before the requested timestamp (this can be changed with the `THRESH` variable).
3. api.py responds to client with JSON data containing the image links and their relative location on the map for every area.

api.py is configured by [config.py](./config.py.skel).


### run_scrapers.py: The file that runs the scraper to get the images (must run this file)

This file utilises the [scraper.py](./scraper.py) file in order to collect images from the internet.

This file, when executed, will check and create directories in `./static/img/`.

Change the `START_DATE` variable in this file to reconfigure where the scraper will start fetching images from.


### areas.json: The file that defines the areas on the map and their imagery source

The following is a description of each of the necessary fields:

```
{
  "areas" : {

    // This defines an area, it can be given any name
    "AUS" : {

      // URLS for scraping, replace "{}" with where the timestamp must go.
      // These are optional but at least one should be included.
      "scraper-url-radar"     : "https://URL-to-images/{}.png",
      "scraper-url-cloud"     : "https://URL-to-images/{}.png",
      "scraper-url-lightning" : "https://URL-to-images/{}.png",

      // The time between images
      "scraper-iter"          : 600,

      // The location of the area the image should cover on the map
      // SW is the most southwest longitude and latitude
      // NE is the most northeast longitude and latitude
      "location"              : {
        "SW"  :   {"lat" : -47.5031, "lng" : 105.6594},
        "NE"  :   {"lat" : -6.2611 , "lng" : 162.0119}
      },

      // The directories where the images will be stored
      // Make sure to include any preceeding directories
      // Make sure to include a directory ending in "radar", "cloud" and "lightning"
      "directories"           : [
        "./static/img/AU",
        "./static/img/AU/national",
        "./static/img/AU/national/radar",
        "./static/img/AU/national/cloud",
        "./static/img/AU/national/lightning"
      ]
    }
  }
}  
```


### areas.py: Functions that make dealing with areas easier

### config.py: Config file, required for running the API

Copy [config.py.skel](./config.py.skel) to a new file named `config.py` and edit the fields to your desire.

### make_gifs.py: For fun, turns the imagery into a gif animation

### run.sh: Old run file that was meant to run everything in one go, now not working

### scraper.py: Scraper functions used for fetching images


## 4. Adding New Imagery

1. Add a comma to the end of the last region in the [areas.json](./areas.json) file and create a new entry filling out all necessary fields. (use the guide in section 3 (above) for areas.json)

2. In [run_scrapers.py](./run_scrapers.py), copy and paste the following within the `asyncio.gather()` function:

```
scraper.scrape(a.get_area_dir('NAME', type='radar'),
                    start=START_DATE,
                    url=a.get_area_url('NAME', type='radar'),
                    _iter=a.get_area_iter('NAME', type='radar')),
```

Change `NAME` to the name you gave your region in the areas.json file and change radar to `cloud` or `lightning` to change the different imagery sources.

3. Run [run_scrapers.py](./run_scrapers.py) and start scraping some imagery.

---

*Documentation and code was written by Marcus Belcastro (delta1512) for use in the WSU Solar Car Race and related marketing, information or media.*
