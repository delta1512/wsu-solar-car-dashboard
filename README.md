# WSU Solar Car Dashboard

## Execution

Run `run_scrapers.py` and `api.py` simultaneously.

Access the dashboard at: [http://localhost:5000/dashboard](http://localhost:5000/dashboard) (or specify your own port)

Note: All timestamp values are in AEST except those in the hyperlinks found in debugging output.

## Constants and Configuration

### In [run_scrapers.py](./run_scrapers.py)

Change `START_DATE` to a valid AEST timestamp in the form of: `Yr Mnth Day Hour Min` to start the scraper a period back in time. Note that for the URLs provided, images date back to around 1 week beforehand.

### Creating a config.py file

The scripts provided require there be a configuration file named `config.py`. Simply copy [config.py.skel](./config.py.skel) and edit the following:

Change the `HOSTNAME` and `PORT` to desired values for the application.

Change the `MAP_API_KEY` for a different Google map API key.

## Dependencies and Installation

### In General

* Requires Python 3.6 or up
* Requires `Flask` and `aiohttp` Python packages

### CentOS

```
$ sudo yum install pip3.6
$ sudo pip3.6 install flask aiohttp
```
