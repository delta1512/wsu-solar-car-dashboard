import asyncio
import os

import scraper
import areas as a


START_DATE = '201907010130' #AEST: when to start scraping from


#Check for file structre and make necessary directories
directories = ['./static', './static/img'] + a.get_all_dirs()


#Attempt to create the directories if they don't exist
try:
    for directory in directories:
        if not os.path.isdir(directory):
            os.mkdir(directory)
except:
    print('ERROR: Failed to create necessary directories, check file permissions.')
    exit(1)


async def main():
    await asyncio.gather(
        #gather the national radar map
        scraper.scrape(a.get_area_dir('AUS', type='radar'),
                        start=START_DATE,
                        url=a.get_area_url('AUS', type='radar')),

        #gather Darwin NT map
        scraper.scrape(a.get_area_dir('AU_NT_DW', type='radar'),
                        start=START_DATE,
                        url=a.get_area_url('AU_NT_DW', type='radar'),
                        _iter=a.get_area_iter('AU_NT_DW', type='radar')),

        #gather Alice Springs NT map
        scraper.scrape(a.get_area_dir('AU_NT_AS', type='radar'),
                        start=START_DATE,
                        url=a.get_area_url('AU_NT_AS', type='radar'),
                        _iter=a.get_area_iter('AU_NT_AS', type='radar')),

        #gather the Perth WA map
        scraper.scrape(a.get_area_dir('AU_WA_PR', type='radar'),
                        start=START_DATE,
                        url=a.get_area_url('AU_WA_PR', type='radar'),
                        _iter=a.get_area_iter('AU_WA_PR', type='radar')),
    )


#Begin running the scrapers
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
