var mapObj
var imageOverlays = []
var ausBounds


function initMap() {
  const properties = {
    center:new google.maps.LatLng(-23.7, 133.87), //Start at Alice Springs
    zoom:5,
  }
  ausBounds = new google.maps.LatLngBounds(
    {lat: -47.5031, lng: 105.6594}, //South-westernmost
    {lat: -6.2611 , lng: 162.0119}  //North-easternmost
  )

  mapObj = new google.maps.Map(document.getElementById("reg-map"), properties)

  //After loading, try fetching an overlay
  getNewOverlay()
}


function getNewOverlay() {
  var slider = document.getElementById("time-slider")
  displayTimestamp(slider.value)

  httpGet(hostname + "getimages?t=" + Math.round(slider.value), setMapOverlay)
}


function setMapOverlay(overlaysJson) {
  let areas = overlaysJson["areas"]
  let imagery = [overlaysJson["radar"], overlaysJson["cloud"], overlaysJson["lightning"]]
  let latLngs = overlaysJson["latlngs"]
  removeOverlays()

  for (let i = 0; i < areas.length; i++) {
    for (let j = 0;  j < imagery.length; j++) {
      //Setup new image overlay
      currentArea = areas[i]
      latLngData = latLngs[currentArea]

      imageOverlays.push(
        new google.maps.GroundOverlay(
          imagery[j][currentArea],
          new google.maps.LatLngBounds(
              latLngData["SW"],  //South-westernmost
              latLngData["NE"]   //North-easternmost
          )
        )
      )
    }
  }

  //Set all the image overlays onto the map
  for (let i = 0; i < imageOverlays.length; i++) {
    imageOverlays[i].setMap(mapObj)
  }
}


//Removes all map overlays from the imageOverlays array and
//clears the images from the map
function removeOverlays() {
  while (imageOverlays[0]) {
    imageOverlays.pop().setMap(null)
  }
}


//Function from Joan on StackOverflow
//https://stackoverflow.com/questions/247483/http-get-request-in-javascript
function httpGet(url, callback) {
    var xmlHttp = new XMLHttpRequest()
    xmlHttp.responseType = 'json';
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          callback(xmlHttp.response)
        }
    }
    xmlHttp.open("GET", url, true)
    xmlHttp.send(null)
}
