from os import popen
from glob import glob
import areas


for area_dir in areas.get_all_dirs():
    pngs = '{}/*.png'.format(area_dir)
    if len(glob(pngs)) < 2:
        continue
    print('redering gif for: {}'.format(area_dir))
    out_dir = input('Output file name (enter to skip): ')
    if out_dir == '':
        print('Skipped')
        continue
    popen('convert -dispose Background -limit memory 16000000 -limit map 160000000 {} {}'.format(pngs, out_dir))
