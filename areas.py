import json

#Fetch the json from the provided file
with open('areas.json', 'r') as area_json:
    AREAS = json.load(area_json)['areas']


def get_all_dirs():
    dirs = []
    for area in AREAS:
        for directory in AREAS[area]['directories']:
            dirs.append(directory)
    return dirs


def get_area_dir(area, type='radar'):
    for directory in AREAS[area]['directories']:
        if directory.endswith(type):
            return directory


def get_area_url(area, type='radar'):
    return AREAS[area]['scraper-url-{}'.format(type)]


def get_area_iter(area, type='radar'):
    return AREAS[area]['scraper-iter']


def get_area_latlng(area):
    return AREAS[area]['location']
